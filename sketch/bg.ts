function drawBg(bg: any) {
    bg.background("#020202");

    if (!MOBILE)
        for (let i = 0; i < 1e5; i++) {
            bg.stroke(random(10, 200))
            bg.strokeWeight(random(0, 2))
            bg.point(random(-bg.width, bg.width), random(-bg.height, bg.height), random(0, -300))
        }
}