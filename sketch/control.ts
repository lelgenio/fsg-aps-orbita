var TOG_TIMEOUT = 0
function controlls() {
    if (keyIsPressed) {
        if (key == "d") hub.acc(createVector(20, 0))
        if (key == "a") hub.acc(createVector(-20, 0))
        if (key == "s") hub.acc(createVector(0, 20))
        if (key == "w") hub.acc(createVector(0, -20))
        if (key == ".") TSCALE += 100
        if (key == ",") TSCALE -= 100
    }

    // console.log(mouseIsPressed)
    if (mouseIsPressed) {
        var hDiff = mouseX / width - .5
        var vDiff = mouseY / height - .5
        if (hDiff > .45) {
            TSCALE -= int(vDiff * 20)
            if (abs(vDiff) < .01) {
                TSCALE = 1
            }
        } else if (hDiff < -.45 && vDiff < .45) {
            if (TOG_TIMEOUT == 0) {
                DEBUG = !DEBUG;
                TOG_TIMEOUT = 10;
            }
        } else {
            var vec = createVector(hDiff, vDiff)
            vec.setMag(20)
            hub.acc(vec)
        }
    }
    if (TOG_TIMEOUT)
        TOG_TIMEOUT -= 1;
}

function keyPressed() {
    if (key == "i") DEBUG = !DEBUG
}

function printInfo() {
    push()
    translate(-width / 2.7, -height / 2.7, 200)
    // textAlign(CENTER)
    text('TEMPO * {scale}'.replace('{scale}', TSCALE.toString()), 0, 0)
    pop()
}