class corpo {
    pos: p5.Vector; // pos em m
    vel: p5.Vector; // velocidade em m/s
    rot: number; // velocidade em m/s
    mass: number;   // massa em kg
    radius: number; // raio em m
    texture: p5.Image;
    tScale: number;

    constructor(
        pos = createVector(0, 0, 0),
        radius: number,
        mass: number,
        tScale: number = null,
        texture: p5.Image = null,
    ) {
        this.pos = pos;
        this.vel = createVector(0, 0, 0);
        this.mass = mass;
        this.radius = radius;
        this.rot = 0;
        this.texture = texture;
        this.tScale = tScale;
    }


    drawPos() {
        return this.pos.copy().div(
            362e6 / min(windowWidth / 3, windowHeight / 3)
        );
    }
    draw(): void {
        // TODO:
        // suporte a imagens
        // imageMode(CENTER);
        // let earthIco = loadImage("pics/worldwide.png");
        // image(earthIco, this.pos.x, this.pos.y, this.mass, this.mass);

        noStroke()
        ambientMaterial(255, 255, 255)

        push()
        pointLight(255, 255, 255, lightPos)
        translate(this.drawPos());
        if (this.texture)
            texture(this.texture);
        rotateZ(-this.rot)
        rotateX(-HALF_PI)
        sphere(this.radius);
        pop()
    }


    draw_debug(others: Array<corpo>) {
        var force = createVector(0, 0);
        for (const other of others) {
            if (other.pos != this.pos) {
                force.add(this.getGForce(other));
            }
        }

        strokeWeight(3);

        var att = force.copy().mult(2e4)
        att.setMag(sqrt(att.mag()))
        att.setMag(att.mag() + this.radius)
        stroke(0, 255, 0);
        vectorLine(this.drawPos(), att);

        var spd = this.vel.copy().div(40)
        spd.setMag(spd.mag() + this.radius)
        stroke(0, 0, 255);
        vectorLine(this.drawPos(), spd);

        dbg_cords(this, this.radius)

        noStroke();

    }


    move(others: Array<corpo>): void {
        var tScale;
        if (!this.tScale)
            tScale = TSCALE;
        else
            tScale = this.tScale;

        for (const other of others) {
            //  evita o objeto atrair a ele mesmo
            if (other != this) {
                var force = this.getGForce(other);
                this.vel.add(force.mult(tScale));
            }
        }
        this.pos.add(this.vel.copy().mult(tScale))
        this.rot += tScale / (24 * 60 * 60);
        this.vel.z = 0
        this.pos.z = 0
    }


    /*
        dist(other: corpo):number{
            // diferença em x
            var xD = abs(this.pos.x - other.pos.x);
            // diferença em y
            var yD = abs(this.pos.y - other.pos.y);

            // retorna o resultado do através do Teorema de Pitágoras
            return sqrt(pow(xD, 2) + pow(yD, 2));
        }
    */


    getGForce(other: corpo): p5.Vector {
        // // Constante Universal Gravitacional
        let G = 6.67e-11;

        // // Distância entre corpos
        // // diferença em x
        // var xD = this.pos.x - other.pos.x;
        // // diferença em y
        // var yD = this.pos.y - other.pos.y;

        // //Força de aceleração
        // let xF = this.mass * other.mass / pow(xD, 2);
        // let yF = this.mass * other.mass / pow(yD, 2);

        var force = other.pos.copy().sub(this.pos);
        if (force.mag() == 0)
            return force
        force.setMag(
            this.mass * other.mass * G
            / pow(force.mag(), 2)
        );
        force.div(this.mass);
        return force;
    }

}

function dbg_cords(obj: corpo, pad: number) {
    push()
    {
        translate(obj.drawPos());
        stroke("red")
        point(pad - 5, -pad - 5, 0)
        var s = "x:{x}Gm\ny:{y}Gm"
        s = s.replace('{x}', (obj.pos.x / 1e6).toFixed(2));
        s = s.replace('{y}', (obj.pos.y / 1e6).toFixed(2));
        s = s.replace('{z}', (obj.pos.z / 1e6).toFixed(2));
        text(s, pad, - pad, 0);
    }
    pop()
}

// desenja uma linha de um vetor ate o outro
function vectorLine(v1: p5.Vector, v2: p5.Vector) {
    line(v1.x, v1.y, v1.z,
        v1.x + v2.x,
        v1.y + v2.y,
        v1.z + v2.z
    );
}

function absLine(v1: p5.Vector, v2: p5.Vector) {
    line(v1.x, v1.y, v1.z,
        v2.x, v2.y, v2.z);
}
