var DEBUG = false;
var MOBILE = true;
var TSCALE = 1500;
var rot: p5.Vector;
var font: p5.Font;
var textEarth: p5.Image;
var textMoon: p5.Image;
var objs: Array<corpo> = [];
var earth: corpo, moon: corpo, hub: sat;
var lightPos: p5.Vector;
var bg: any;

function preload() {
    font = loadFont('fonts/Roboto-Regular.ttf');
    textEarth = loadImage('pics/earth.jpg')
    textMoon = loadImage('pics/moon.jpg')
    bg = createGraphics(windowWidth * 2, windowHeight * 2)
}

function setup() {
    createCanvas(windowWidth, windowHeight, WEBGL);
    windowResized()
    textFont(font)

    lightPos = createVector(3000, -50, 0);
    rot = createVector(0, 0);

    // corpo(pos,radio,mass)
    earth = new corpo(createVector(0, 0, 0), 35, 5.97237e24, null, textEarth);
    moon = new corpo(createVector(0, 362e6, 0), 15, 7.342e22, null, textMoon);
    hub = new sat(createVector(0, -150e6, 0), 7, 1);
    moon.vel.add(1022, 0, 0);
    hub.vel.add(1300, 0, 0)
    objs = [earth, moon, hub];
    frameRate(30)
}

function draw() {
    clear()
    if (DEBUG)
        background('#202020')
    else
        image(bg, -width, -height, width * 2, height * 2)

    controlls()

    for (const obj of objs) {
        obj.move(objs);
        obj.draw()
        if (DEBUG) {
            obj.draw_debug(objs);
            strokeWeight(1);
            stroke("yellow");
            absLine(obj.drawPos(), lightPos)
        }
    }


    if (DEBUG) {
        strokeWeight(2);
        stroke("green")
        vectorLine(createVector(0, 0, 0), createVector(100000, 0, 0));
        stroke("blue")
        vectorLine(createVector(0, 0, 0), createVector(0, -100000, 0));
        stroke("red")
        vectorLine(createVector(0, 0, 0), createVector(0, 0, 100000));
    }

    printInfo()
}


function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
    if (!DEBUG) {
        bg.resizeCanvas(windowWidth * 2, windowHeight * 2);
        drawBg(bg)
    }

    if (width > height * 1.2)
        MOBILE = false

    drawBg(bg)
}
