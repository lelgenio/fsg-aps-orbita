var PREV: number
var P_TSCALE: number
class sat extends corpo {
    t: next;
    draw() {
        super.draw()
        if (MOBILE) {
            PREV = 50;
            P_TSCALE = 10000;
        }
        else {
            PREV = 300
            P_TSCALE = TSCALE;
        }
        this.t = new next(this.pos, this.vel, PREV)
    }
    acc(dir: p5.Vector) {
        dir.rotate(this.vel.heading() + HALF_PI)
        this.vel.add(dir)
        for (let i = 0; i < 5; i++) {
            push()
            translate(this.drawPos())
            pointLight(255, 255, 255, lightPos)

            var noise = p5.Vector.random3D().setMag(dir.mag() * 0.3)
            p5.Vector.lerp(dir, noise, 0.3, dir)
            var ang = dir.heading() - HALF_PI
            rotateZ(ang)

            normalMaterial()
            ambientMaterial(255, 0, 0, 10)
            cone(10, 40)
            pop()
        }
    }
}
class next extends corpo {
    depth: number; //profundidade
    child: next; //filho
    constructor(pos: p5.Vector, vel: p5.Vector, depth: number) {
        super(pos.copy(), 1, 1, 5e3)
        this.vel = vel.copy()
        this.depth = depth
        this.tScale = 10000

        this.move(objs)

        var touch = false
        for (const iterator of objs) {
            var a = this.drawPos()
            var b = iterator.drawPos()
            if (dist(a.x, a.y, b.x, b.y) < iterator.radius && iterator != hub) {
                var touch = true
            }
        }
        if (this.depth > 0 && !touch) {
            this.child = new next(this.pos, this.vel, this.depth - 1)
            stroke(0, 0, 200 * depth / PREV, 200 * depth / PREV)
            strokeWeight(2 * depth / PREV)
            absLine(this.drawPos(), this.child.drawPos())
        }
    }

}